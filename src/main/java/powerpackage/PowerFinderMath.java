package powerpackage;

public class PowerFinderMath {
    public static float FindPowerOfNumber(int base, int power) {
        float result = 1;
        if(power>0) {
            result = FindPositivePowerOfNumber(base,power);
        }
        else if(power<0){
            result = FindNegativePowerOfNumber(base,power);
        }

        return result;
    }

    private static float FindPositivePowerOfNumber(int base, int power){
        float result = 1;
        for (int i = 1; i <= power; i++) {
            result = result * base;
        }
        return result;
    }

    private static float FindNegativePowerOfNumber(int base, int power){
        float result = 1;
        for (int i = -1; i >= power; i--) {
            result = result / base;
        }
        return result;
    }
}