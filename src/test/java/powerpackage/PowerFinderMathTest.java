package powerpackage;

import org.junit.jupiter.api.Test;

public class PowerFinderMathTest {
    @Test
    public void shouldPowerOf1ForBase1Is1() {
        assert PowerFinderMath.FindPowerOfNumber(1,1) == 1;
    }

    @Test
    public void shouldPowerOf1ForBase2Is2() {
        assert PowerFinderMath.FindPowerOfNumber(2,1) == 2;
    }

    @Test
    public void shouldPowerOf2ForBase2Is4() { assert PowerFinderMath.FindPowerOfNumber(2, 2) == 4;}

    @Test
    public void shouldPowerOf2ForBase3Is9() {
        assert PowerFinderMath.FindPowerOfNumber(3, 2) == 9 ;
    }

    @Test
    public void shouldPowerOfMinus2ForBase2Is0Dot25() {
        assert PowerFinderMath.FindPowerOfNumber(2, -2) == 0.25;
    }

    @Test
    public void shouldPowerOfZeroForBase2Is1() {
        assert PowerFinderMath.FindPowerOfNumber(2,0) == 1;
    }

    @Test
    public void shouldPowerOfMinus1ForBaseMinus2IsZeroDot5() { assert PowerFinderMath.FindPowerOfNumber(-2,-1) == -0.5; }

    @Test
    public void shouldPowerOf2ForBaseMinus2Is4() { assert PowerFinderMath.FindPowerOfNumber(-2, 2) == 4; }

    @Test
    public void shouldPowerOfMinus1ForBaseMinus2IsMinus2() { assert PowerFinderMath.FindPowerOfNumber(-2, 1) == -2; }

    @Test
    public void shouldPowerOfZeroForBaseZeroIs1() { assert PowerFinderMath.FindPowerOfNumber(0, 0) == 1; }
}